# rockylinux9_ohpc3.1

Docker image based on:

* Rocky Linux 9.3
* OpenHPC v3.1

with the following packages installed

* ohpc-base,
* gnu13-compilers-ohpc,
* openmpi5-pmix-gnu13-ohpc
* python3.11-numpy-gnu13-ohpc \
* python3.11-mpi4py-gnu13-openmpi5-ohpc
* git

