FROM rockylinux:9.3 

RUN yum -y update ; yum clean all

RUN \
  dnf -y install http://repos.openhpc.community/OpenHPC/3/EL_9/x86_64/ohpc-release-3-1.el9.x86_64.rpm
RUN \
  dnf -y install ohpc-base \
  gnu13-compilers-ohpc \
  openmpi5-pmix-gnu13-ohpc
RUN \
  dnf -y install python3.11-numpy-gnu13-ohpc \
  python3.11-mpi4py-gnu13-openmpi5-ohpc
RUN \
  dnf -y install git

CMD ["/bin/bash"]
# ENTRYPOINT ["/root/entrypoint.sh"]
